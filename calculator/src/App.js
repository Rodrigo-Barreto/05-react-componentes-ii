import React from 'react';
import './App.css';
import {  Button,  } from '@material-ui/core';
import Input from './components/input/input';

import ListData from './components/list/list';




class App extends React.Component {
     
  constructor(props) {
    super(props);

    this.state = {
      dadosSto: [],
      nome: '',
      email: '',
      contato: '',
      descricao: '',
      isTrue: false

    }

  }


  addForm =()=> {
    let verification = this.emailVerification() && this.contatoVerification() && this.emptyVerification()

    let dados = {
      nome: this.state.nome,
      email: this.state.email,
      contato: this.state.contato,
      descricao: this.state.descricao,
    }

    
    if (verification) {
      this.setState({dadosSto:[...this.state.dadosSto,dados]})
    } else {
      alert('Alum Valor errado')
    }
  }

  deleteForm =(item)=>{
    let searc = this.state.dadosSto.indexOf(item)
    const { state: {  dadosSto } } = this
    dadosSto.splice(searc, 1)
    this.setState({  dadosSto })
  }

  test(){
    alert(this.state.dadosSto)
  }
  //VERIFICATIONS 
  emailVerification() {
    const verification = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (verification.test(this.state.email)) {
      return true;
    } else {
      return false;
    }
  }

  contatoVerification() {
    let number = this.state.contato;

    if (!isNaN(number) && number.length == 9) {
      return true;
    } else {
      return false;
    }
  }
  emptyVerification() {
    let name = this.state.nome
    let description = this.state.descricao

    if (name && description) {
      return true;
    } else {
      return false;
    }
  }
  //VERIFICATIONS 




  render() {
    return <div >

      
      
      <br></br>
      <Input nome='Nome' changeValue={(value) => this.setState({ nome: value })} value={this.state.nome} ></Input>
      <br></br>
      <Input nome='Email' changeValue={(value) => this.setState({ email: value })} value={this.state.email} ></Input>
      <br></br>
      <Input nome='Contato' changeValue={(value) => this.setState({ contato: value })} ></Input>
      <br></br>

      <Input nome='Descricao' changeValue={(value) => this.setState({ descricao: value })} value={this.state.descricao} ></Input>
      {/* startIcon={<ShoppingCartRounded />} */}
      <br></br>

      <Button onClick={() => this.addForm()} variant="contained" disabled={this.state.isTrue}> Enviar</Button>
     
     {/* { this.state.dadosSto.map((e)=><p>{e.nome}</p>)} */}

     <ListData  deleteForm={this.deleteForm} data={this.state.dadosSto}/>

    </div>

  }


}

export default App;
