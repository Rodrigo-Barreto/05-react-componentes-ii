import { TextField } from "@material-ui/core";
import React from "react";

const Input = props => {
    return (

        <TextField  variant="outlined" label={props.nome}   type="text" value={props.value} onChange={event => { props.changeValue(event.target.value) }}   ></TextField>


    )
}

export default Input